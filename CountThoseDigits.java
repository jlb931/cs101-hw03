/**
 * Generates 20 random numbers between 0 and 9, then determines the count of each of the numbers generated.
 * 
 * @author Jack Booth
 * @version 10/4/16
 */
public class CountThoseDigits
{
	public static void main(String[] args)
	{
//		Generate 20 random numbers and put them in an array
		int[] randomArray = new int[20];
		for(int i = 0; i < 20; i++)
		{
			randomArray[i] = (int)(Math.random()*10);
		}
		
//		Sort through the numbers, and count one in the corresponding index of a second array if that number is in the random array.
		int[] sortedArray = new int[10];
		for(int i = 0; i < 10; i++)
		{
			for(int k = 0; k < 20; k++)
			{
				if(randomArray[k] == i)
				{
					sortedArray[i] += 1;
				}
			}
		}
		
//		Print out the counts stored in the sorted array.
		for(int i = 0; i < 10; i++)
		{
			if(sortedArray[i] == 0)
				System.out.println("There were no " + i + "s");
			else if(sortedArray[i] == 1)
				System.out.println("There was 1 " + i);
			else
				System.out.println("There were " + sortedArray[i] + " " + i + "s");
		}
	}
}
